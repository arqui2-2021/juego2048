package j2048backend;
import j2048frontend.Observador;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Tablero {
    private int [][]tablero;
    private final int dimension = 4;
    private final int LimiteCasilla=16;
    private final List<Observador> observadores;
    public  Tablero() {
        tablero = new int[dimension][dimension];
        this.observadores = new ArrayList<>();
    }
    public Estado estado(){
        Estado estado = null;
        if(alcanceLimite()){
            estado = Estado.GANADO;
        }else if (sePuedeMover()){
            estado = Estado.CONTINUAR;
        }else {
            estado = Estado.PERDIDO;
        }
        return estado;
    }
    private boolean casillaVacia(int column, int fila){
        return tablero[fila][column] == 0;
    }
    public boolean tableroLLeno(){// toString
        boolean res = true;
        for (int[] ints : tablero) {
            for (int anInt : ints) {
                if (anInt == 0) {
                    res = false;
                    break;
                }
            }
        }
        return !res;
    }
    public  boolean insertarNumeroDos(){
        boolean res = false;
        int columna = (int)(Math.random()*(dimension));
        int fila = (int)(Math.random()*(dimension));
        if(tableroLLeno()){
            while (!casillaVacia(columna,fila)){
                columna = (int)(Math.random()*(dimension));
                fila = (int)(Math.random()*(dimension));
            }
            tablero[fila][columna] = 2;
            res = true;

        }
        return res;
    }
    public String toString(){
        StringBuilder res= new StringBuilder();
        for (int i =0 ; i< dimension ; i++) {
            for (int j = 0; j < tablero[i].length; j++) {
                if (j < tablero[i].length - 1) {
                    res.append(tablero[i][j]).append(" ");
                } else {
                    res.append(tablero[i][j]);
                }
            }
            if (i < tablero[i].length - 1) {
                res.append("|");
            }
        }
        return res.toString();
    }
    public void addTablero(int [][] tab){
        tablero = tab;
    }

    private boolean adyacenteFila(int ini , int fin , int direccion , int num, int fila){//direccion = +1 , -1
        int numOriginal = num;
        while (ini != fin ){
            num = tablero[fila][ini];
            if (numOriginal != num || numOriginal == 0) {
                if(numOriginal != num && num !=0) {
                    return false;
                }
            } else {
                return true;
            }
            ini = ini+direccion;
        }
        return false;
    }
    private boolean adyacenteColumna(int ini , int fin , int direccion , int num, int columna){//direccion = +1 , -1
        int numOriginal = num;
        while (ini != fin ){
            num = tablero[ini][columna];
            if (numOriginal != num || numOriginal == 0) {
                if(numOriginal != num && num !=0) {
                    return false;
                }
            } else {
                return true;
            }
            ini = ini+direccion;
        }
        return false;
    }

    private int posColumna(int ini , int fin , int direccion , int num, int fila){
        int numOriginal = num;
        while(ini != fin ){
                num = tablero[fila][ini];
                if (numOriginal != num || numOriginal == 0) {
                    if(numOriginal != num && num !=0){
                        return 0;
                    }
                } else {
                    return ini;
                }
                ini = ini+direccion;
            }
        return 0;
    }
    private int posFila(int ini , int fin , int direccion , int num, int columna){
        int numOriginal = num;
        while(ini != fin){
                num = tablero[ini][columna];
                if (num != numOriginal || numOriginal == 0) {
                    if(numOriginal != num && num !=0){
                        return 0;
                    }
                } else {
                    return ini;
                }
                ini = ini+direccion;
        }
    return 0;
}

    private int[]  moverTablero(int num,int i, int j,String posicion){
        int numAux = num;
        int [] lista = new int[dimension];
        int k=0;
        switch (posicion) {
            case "Left" -> {
                if (j >= dimension) {
                    lista[k] = numAux;
                    numAux = 0;
                    j++;
                }
                while (j < dimension) {
                    num = tablero[i][j];
                    int suma = 0;
                    if (num != 0 || adyacenteFila(j,dimension,1,numAux,i)) {
                        if (adyacenteFila(j,dimension,1,numAux,i)) {
                            j = posColumna(j,dimension,1,numAux,i);
                            num = tablero[i][j];
                            suma = numAux + num;
                            lista[k] = suma;
                            j++;
                            if (j < dimension) {
                                numAux = tablero[i][j];
                                k++;
                            } else {
                                numAux = 0;
                            }
                        }
                        else if (numAux != 0) {
                            lista[k] = numAux;
                            numAux = num;
                            k++;
                        } else {
                            numAux = tablero[i][j];
                        }

                    } else if (numAux != 0) {
                        lista[k] = numAux;
                        numAux = num;
                        k++;
                    } else {
                        numAux = tablero[i][j];
                    }
                    j++;
                }
                if (numAux != 0) {
                    lista[k] = numAux;
                }
            }
            case "Right" -> {
                k = dimension - 1;
                if (j <= -1) {
                    lista[k] = numAux;
                    numAux = 0;
                    j--;
                }
                while (j >= 0) {
                    num = tablero[i][j];
                    int suma = 0;
                    if (0 != num || adyacenteFila(j,-1,-1,numAux,i)) {
                        if (adyacenteFila(j,-1,-1,numAux,i)) {
                            j = posColumna(j,-1,-1,numAux,i);
                            num = tablero[i][j];
                            suma = numAux + num;
                            lista[k] = suma;
                            j--;
                            if (j >= 0) {
                                numAux = tablero[i][j];
                                k--;
                            } else {
                                k--;
                                numAux = 0;
                            }
                        } else {
                            if (numAux == 0) {
                                numAux = tablero[i][j];
                            } else {
                                lista[k] = numAux;
                                numAux = num;
                                k--;
                            }
                        }

                    } else if (numAux != 0) {
                        lista[k] = numAux;
                        numAux = num;
                        k--;
                    } else {
                        numAux = tablero[i][j];
                    }
                    j--;
                }
                if (numAux != 0) {
                    lista[k] = numAux;
                }
            }
            case "Up" -> {
                if (i >= dimension) {
                    lista[k] = numAux;
                    numAux = 0;
                    i++;
                }
                while (i < dimension) {
                    num = tablero[i][j];
                    int suma = 0;
                    if (num != 0 || adyacenteColumna(i ,dimension, 1 ,numAux ,j)) {
                        if (adyacenteColumna(i ,dimension, 1 ,numAux ,j)) {
                            i = posFila(i ,dimension, 1 ,numAux ,j);
                            num = tablero[i][j];
                            suma = numAux + num;
                            lista[k] = suma;
                            i++;
                            if (i < dimension) {
                                numAux = tablero[i][j];
                                k++;
                            } else {
                                numAux = 0;
                            }
                        } else if (numAux != 0) {
                            lista[k] = numAux;
                            numAux = num;
                            k++;
                        } else {
                            numAux = tablero[i][j];
                        }

                    } else if (numAux != 0) {
                        lista[k] = numAux;
                        numAux = num;
                        k++;
                    } else {
                        numAux = tablero[i][j];
                    }
                    i++;
                }
                if (numAux != 0) {
                    lista[k] = numAux;
                }
            }
            case "Down" -> {
                k = dimension - 1;
                if (i <= -1) {
                    lista[k] = numAux;
                    numAux = 0;
                    i--;
                }
                while (i >= 0) {
                    num = tablero[i][j];
                    int suma = 0;
                    if (num != 0 || adyacenteColumna(i ,-1, -1 ,numAux ,j)) {
                        if (adyacenteColumna(i ,-1, -1 ,numAux ,j)) {
                            i = posFila(i ,-1, -1 ,numAux ,j);
                            num = tablero[i][j];
                            suma = numAux + num;
                            lista[k] = suma;
                            i--;
                            if (i >= 0) {
                                numAux = tablero[i][j];
                                k--;
                            } else {
                                k--;
                                numAux = 0;
                            }
                        } else if (numAux != 0) {
                            lista[k] = numAux;
                            numAux = num;
                            k--;
                        } else {
                            numAux = tablero[i][j];
                        }

                    } else if (numAux != 0) {
                        lista[k] = numAux;
                        numAux = num;
                        k--;
                    } else {
                        numAux = tablero[i][j];
                    }
                    i--;
                }
                if (numAux != 0) {
                    lista[k] = numAux;
                }
            }
            default -> throw new IllegalStateException("Unexpected value: " + posicion);
        }
        return lista;
    }
    private void modificarTablero(int [] lista ,int i ,int j, String mov){
        if (!Objects.equals(mov, "row")) {
            for(int k = 0 ; k < lista.length;k++){
                tablero [k][j]=lista[k];
            }
        } else {
            tablero[i] = lista;
        }
    }
    public void mover(String movimiento){
        for(int i = 0 ; i < dimension ;i++){
            for(int j = 0 ; j < dimension; j++){
                int obtenerCasilla=0;
                int aux = 0;
                int [] lista = new int[dimension];
                switch (movimiento) {
                    case "Left" -> {
                        obtenerCasilla = tablero[i][j];
                        lista = tablero[i];
                        if (obtenerCasilla != 0) {
                            lista = moverTablero(obtenerCasilla, i, j + 1, movimiento);
                            modificarTablero(lista, i, j, "row");
                        }
                    }
                    case "Right" -> {
                        aux = (dimension - 1) - j;
                        obtenerCasilla = tablero[i][aux];
                        if (obtenerCasilla != 0) {
                            lista = moverTablero(obtenerCasilla, i, aux - 1, movimiento);
                            modificarTablero(lista, i, j, "row");
                        }
                    }
                    case "Up" -> {
                        aux = j;
                        obtenerCasilla = tablero[aux][i];
                        if (obtenerCasilla != 0) {
                            lista = moverTablero(obtenerCasilla, aux + 1, i, movimiento);
                            modificarTablero(lista, aux, i, "column");
                        }
                    }
                    case "Down" -> {
                        aux = (dimension - 1) - j;
                        obtenerCasilla = tablero[aux][i];
                        if (obtenerCasilla != 0) {
                            lista = moverTablero(obtenerCasilla, aux - 1, i, movimiento);
                            modificarTablero(lista, aux, i, "column");
                        }
                    }
                }
                if(obtenerCasilla != 0){
                    break;
                }
            }
        }
    }

    //movimientos
    public void  moverIzquierda() {
        String cad_original = toString();
        mover("Left");
        if(!cad_original.equals(toString())){
            insertarNumeroDos();
        }
        notificar();
    }

    public void moverDerecha() {
        String cad_original = toString();
        mover("Right");
        if(!cad_original.equals(toString())){
            insertarNumeroDos();
        }
        notificar();
    }

    public void moverArriba() {
        String cad_original = toString();
        mover("Up");
        if(!cad_original.equals(toString())){
            insertarNumeroDos();
        }
        notificar();
    }

    public void moverAbajo() {
        String cad_original = toString();
        mover("Down");
        if(!cad_original.equals(toString())){
            insertarNumeroDos();
        }
        //if(){}
        notificar();
    }

    // ganar o perder
    private boolean alcanceLimite() {//en el toString preguntar 2048
        for (int[] lista : tablero) {
            for (int k : lista) {
                if (k == LimiteCasilla) {
                    return true;
                }
            }
        }
        return false;
    }
    private boolean sePuedeMover(){
        boolean res = false;
        if(tableroLLeno()){
            return true;
        }else{
            for(int i = 0 ; i < dimension-1 ; i++){
                for(int j = 0 ; j < dimension-1 ; j++){
                    int casilla = tablero[i][j];
                    if(adyacenteFila(j,dimension,1,casilla,i) ||
                            adyacenteColumna(i ,dimension, 1 ,casilla ,j)){
                        return true;
                    }
                }

            }
        }
        return res;
    }

    public void agregarObservador(Observador observador) {
        this.observadores.add(observador);
    }
    public void notificar() {
        Estado evento = estado();
        for (Observador observador: observadores){
            observador.actualizar(evento);
        }
    }
}