package j2048backend;

import java.util.Objects;

public class Direccion {
    private  int [][] tablero;
    int dimension = 4;
    public Direccion(String cad_tablero) {
        covertirTablero(cad_tablero);
    }

    private void ponerIntArr(String [] cad1, int k){
        for(int i = 0 ;i<dimension;i++){
            tablero [k][i] = Integer.parseInt(cad1[i]);
        }
    }
    private void covertirTablero(String cad){
        String [] cad1 = cad.split("\\|");
        int i = 0 ;
        for(String key : cad1){
            String [] cad2 = key.split(" ");
            ponerIntArr(cad2, i);
            i++;
        }
    }

    private boolean numPar(int num,int fila , int columna, String pos){
        int numOriginal = num;
        if(Objects.equals(pos, "Left")){
            while(!(dimension <= columna)){
                num = tablero[fila][columna];
                if(numOriginal == num && numOriginal !=0 ){
                    return true;
                }else if(numOriginal != num && num !=0){
                    return false;
                }
                columna++;
            }
        }else{
            while(columna >= 0 ){
                num = tablero[fila][columna];
                if(numOriginal == num && numOriginal !=0 ){
                    return true;
                }else if(numOriginal != num && num !=0){
                    return false;
                }
                columna--;
            }
        }
        return false;
    }
    private boolean numParColumn(int num,int i , int j, String pos){
        int numOriginal = num;
        if(Objects.equals(pos, "Up")){
            while(i < dimension){
                num = tablero[i][j];
                if(numOriginal == num && numOriginal !=0 ){
                    return true;
                }else if(numOriginal != num && num !=0){
                    return false;
                }
                i++;
            }
        }else{
            while(i >=0){
                num = tablero[i][j];
                if(numOriginal == num && numOriginal !=0 ){
                    return true;
                }else if(numOriginal != num && num !=0){
                    return false;
                }
                i--;
            }
        }
        return false;
    }

    private int posColumna(int num, int fila , int column, String pos){
        int res = 0;
        int numOriginal = num;
        if(Objects.equals(pos, "Left")){

            while(!(dimension <= column)){
                num = tablero[fila][column];
                if(numOriginal == num && numOriginal !=0 ){
                    return column;
                }else if(numOriginal != num && num !=0){
                    return 0;
                }
                column++;
            }
        }else{


            while(column >= 0 ){
                num = tablero[fila][column];
                if (numOriginal != num || numOriginal == 0) {
                    if(numOriginal != num && num !=0){
                        return 0;
                    }
                } else {
                    return column;
                }
                column--;
            }

        }
        return 0;
    }
    private int posFila(int num, int fila , int column, String pos){
        int res = 0;
        int numOriginal = num;
        if (!Objects.equals(pos, "Up")) {


            while(fila >= 0 ){
                num = tablero[fila][column];
                if(numOriginal == num && numOriginal !=0 ){
                    return fila;
                }else if(numOriginal != num && num !=0){
                    return 0;
                }
                fila--;
            }

        } else {

            while(fila < dimension){
                num = tablero[fila][column];
                if(numOriginal == num && numOriginal !=0 ){
                    return fila;
                }else if(numOriginal != num && num !=0){
                    return 0;
                }
                fila++;
            }
        }
        return 0;
    }
    public int[]  moverTablero(int num,int i, int j,String posicion){
        int numAux = num;
        int [] lista = new int[dimension];
        int k=0;
        switch (posicion) {
            case "Right" -> {
                k = dimension - 1;
                if (j <= -1 && k == dimension - 1) {
                    lista[k] = numAux;
                    numAux = 0;
                    j--;
                }
                while (j >= 0) {
                    num = tablero[i][j];
                    int suma = 0;
                    if (num != 0 || numPar(numAux, i, j, posicion)) {
                        if (numPar(numAux, i, j, posicion)) {
                            j = posColumna(numAux, i, j, posicion);
                            num = tablero[i][j];
                            suma = numAux + num;
                            lista[k] = suma;
                            j--;
                            if (j >= 0) {
                                numAux = tablero[i][j];
                                k--;
                            } else {
                                k--;
                                numAux = 0;
                            }
                        } else if (numAux != 0) {
                            lista[k] = numAux;
                            numAux = num;
                            k--;
                        } else {
                            numAux = tablero[i][j];
                        }

                    } else if (numAux != 0) {
                        lista[k] = numAux;
                        numAux = num;
                        k--;
                    } else {
                        numAux = tablero[i][j];
                    }
                    j--;
                }
                if (numAux != 0) {
                    lista[k] = numAux;
                }
            }
            case "Left" -> {
                if (j >= dimension) {
                    lista[k] = numAux;
                    numAux = 0;
                    j++;
                }
                while (j < dimension) {
                    num = tablero[i][j];
                    int suma = 0;
                    if (num != 0 || numPar(numAux, i, j, posicion)) {
                        if (numPar(numAux, i, j, posicion) && j <= dimension - 1) {
                            j = posColumna(numAux, i, j, posicion);
                            num = tablero[i][j];
                            suma = numAux + num;
                            lista[k] = suma;
                            j++;
                            if (j < dimension) {
                                numAux = tablero[i][j];
                                k++;
                            } else {
                                numAux = 0;
                            }
                        } else if (numAux != 0) {
                            lista[k] = numAux;
                            numAux = num;
                            k++;
                        } else {
                            numAux = tablero[i][j];
                        }

                    } else if (numAux != 0) {
                        lista[k] = numAux;
                        numAux = num;
                        k++;
                    } else {
                        numAux = tablero[i][j];
                    }
                    j++;
                }
                if (numAux != 0) {
                    lista[k] = numAux;
                }
            }
            case "Down" -> {
                k = dimension - 1;
                if (i <= -1 && k == dimension - 1) {
                    lista[k] = numAux;
                    numAux = 0;
                    i--;
                }
                while (i >= 0) {
                    num = tablero[i][j];
                    int suma = 0;
                    if (num != 0 || numParColumn(numAux, i, j, posicion)) {
                        if (numParColumn(numAux, i, j, posicion)) {
                            i = posFila(numAux, i, j, posicion);
                            num = tablero[i][j];
                            suma = numAux + num;
                            lista[k] = suma;
                            i--;
                            if (i >= 0) {
                                numAux = tablero[i][j];
                                k--;
                            } else {
                                k--;
                                numAux = 0;
                            }
                        } else if (numAux != 0) {
                            lista[k] = numAux;
                            numAux = num;
                            k--;
                        } else {
                            numAux = tablero[i][j];
                        }

                    } else if (numAux != 0) {
                        lista[k] = numAux;
                        numAux = num;
                        k--;
                    } else {
                        numAux = tablero[i][j];
                    }
                    i--;
                }
                if (numAux != 0) {
                    lista[k] = numAux;
                }
            }
            case "Up" -> {
                if (i >= dimension) {
                    lista[k] = numAux;
                    numAux = 0;
                    i++;
                }
                while (i < dimension) {
                    num = tablero[i][j];
                    int suma = 0;
                    if (num != 0 || numParColumn(numAux, i, j, posicion)) {
                        if (numParColumn(numAux, i, j, posicion) && i <= dimension - 1) {
                            i = posFila(numAux, i, j, posicion);
                            num = tablero[i][j];
                            suma = numAux + num;
                            lista[k] = suma;
                            i++;
                            if (i < dimension) {
                                numAux = tablero[i][j];
                                k++;
                            } else {
                                numAux = 0;
                            }
                        } else if (numAux != 0) {
                            lista[k] = numAux;
                            numAux = num;
                            k++;
                        } else {
                            numAux = tablero[i][j];
                        }

                    } else if (numAux != 0) {
                        lista[k] = numAux;
                        numAux = num;
                        k++;
                    } else {
                        numAux = tablero[i][j];
                    }
                    i++;
                }
                if (numAux != 0) {
                    lista[k] = numAux;
                }
            }
        }
        return lista;
    }
}
