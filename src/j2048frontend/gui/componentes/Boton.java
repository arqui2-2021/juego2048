package j2048frontend.gui.componentes;

import javax.swing.*;
import java.awt.*;

public class Boton {

    public Boton(){}
    private ImageIcon icono(String url){
        ImageIcon imageIcon = new ImageIcon(url);
        Image image = imageIcon.getImage();
        Image newimg = image.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        imageIcon = new ImageIcon(newimg);
        return imageIcon;
    }

    public JButton agregarBoton(String url,String nombre){

        ImageIcon icono = icono(url);
        //Icon b = new ImageIcon(getClass().getResource(url));
        JButton res = new JButton(nombre);
        res.setIcon(icono);
        res.setBackground(Color.BLACK);
        res.setForeground(Color.WHITE);
        res.setMinimumSize(new Dimension(20,100));
        return res;
    }
}
