package j2048frontend.gui;
import j2048backend.Estado;
import j2048backend.Tablero;
import j2048frontend.Observador;
import j2048frontend.TableroUI;
import j2048frontend.gui.componentes.Boton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class TableroGUI extends JPanel implements Observador, TableroUI,ActionListener,KeyListener {

    private Tablero tablero;
    private  int [][] arr = null;
    private JPanel showArr = new JPanel(); // pantalla de área digital
    private int dimension= 4 ;
    private JButton btn_arriba,btn_abajo,btn_izquierda,btn_derecha;
    private JFrame jframe;
    private JPanel panelBotone  = new JPanel ();
    public TableroGUI(Tablero tablero) {
        this.tablero = tablero;
        this.arr = new int [dimension][dimension];
        this.tablero.agregarObservador(this);

    }

    private void ponerIntArr(String [] cad1, int k){
        for(int i = 0 ;i<dimension;i++){
            arr [k][i] = Integer.parseInt(cad1[i]);
        }
    }
    private void covertirTablero(String cad){
        String [] cad1 = cad.split("\\|");
        int i = 0 ;
        for(String key : cad1){
            String [] cad2 = key.split(" ");
            ponerIntArr(cad2, i);
            i++;
        }
    }
    private void mostrarTablero() {

        jframe = new JFrame("2048");
        jframe.setBounds(400, 100, 600, 650);
        JPanel jp = new JPanel();
        jframe.setContentPane(jp);
        jp.setLayout(new BorderLayout());

       // this.instrucciones = labes("mover con el teclado: (←)(↑)(→)(↓)",20,Color.BLACK);
        this.showArr.setLayout(new GridLayout(this.dimension, this.dimension));
        this.showArr.setBackground(Color.black);
        jp.add(showArr, BorderLayout.CENTER);
        JPanel panelBotone = Botones();
        addListeners();
        panelBotone.setBackground(Color.black);
        jp.add(panelBotone, BorderLayout.SOUTH);
        jframe.setVisible(true);
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == btn_arriba) {
            tablero.moverArriba();
        } else if (source == btn_abajo) {
            tablero.moverAbajo();
        } else if (source == btn_izquierda) {
            tablero.moverIzquierda();
        } else if (source == btn_derecha) {
            tablero.moverDerecha();
        }
        covertirTablero(tablero.toString());
        actualizarPantalla();
        Estado estado = tablero.estado();
        actualizar(estado);
        /*switch (estado){
            case PERDIDO :
                pintado("PERDISTE");
                break;
            case GANADO:
                pintado("GANASTE");
                break;
            case CONTINUAR:
                break;
        }*/
        }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e){
        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
                tablero.moverArriba();

                break;
            case KeyEvent.VK_DOWN:
                tablero.moverAbajo();
                break;

            case KeyEvent.VK_LEFT:
                tablero.moverIzquierda();
                break;

            case KeyEvent.VK_RIGHT:
                tablero.moverDerecha();
                break;

        }
        covertirTablero(tablero.toString());
        actualizarPantalla();
        Estado estado = tablero.estado();
        switch (estado){
            case PERDIDO :
                //pintado("PERDISTE");
                break;
            case GANADO:
                //pintado("GANASTE");
                break;
            case CONTINUAR:
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
    private void actualizarPantalla(){
        this.showArr.removeAll();
        for (int i = 0; i < this.dimension; i++){
            for (int j = 0; j < this.dimension; j++){
                Integer temp = arr[i][j];
                JLabel jl = new JLabel(0 == temp ? "" : temp.toString(), SwingConstants.CENTER);
                jl.setForeground(Color.getHSBColor(207,6,128));//poner ellabel de color
                jl.setFont (new Font ("Microsoft Yahei", 0, 50));
                jl.setBorder(BorderFactory.createLineBorder(Color.getHSBColor(207,6,128)));
                this.showArr.add(jl);
            }
        }
        this.showArr.updateUI();
    }

    private void resetar(){
        jframe.setVisible(false);
        jframe.dispose();
        tablero = new Tablero();
        String cadTablero = tablero.toString();
        covertirTablero(cadTablero);
        actualizarPantalla();
        correr();
    }

    private void pintado(String estado){

        this.showArr.removeAll();
        this.panelBotone.removeAll();
        this.panelBotone.setBackground(Color.black);
        this.showArr.setBackground(Color.black);
        JLabel jl = labes(estado,50,Color.getHSBColor(207,6,128));
        this.showArr.add(jl);
        this.showArr.updateUI();
        this.panelBotone.updateUI();
    }
    private JLabel labes(String mensaje, int tam, Color color){
        JLabel jlMensaje = new JLabel(mensaje,SwingConstants.CENTER);
        jlMensaje.setFont (new Font ("Microsoft Yahei", 0, tam));
        jlMensaje.setForeground(color);
        return jlMensaje;
    }
    private JPanel Botones(){
        JPanel res= new JPanel();
        Boton boton = new Boton();
        //poner ruta completa
        btn_izquierda = boton.agregarBoton("C:\\Users\\Arminda\\IdeaProjects\\Practica_1\\src\\j2048frontend\\gui\\iconos/izquierda.png","Izquierda");
        res.add(btn_izquierda);
        btn_derecha = boton.agregarBoton("src/j2048frontend/gui/iconos/derecha.png","derecha");
        res.add(btn_derecha);
        btn_arriba =boton.agregarBoton("src/j2048frontend/gui/iconos/arriba.png","arriba");
        res.add(btn_arriba);
        btn_abajo = boton.agregarBoton("src/j2048frontend/gui/iconos/abajo.png","abajo");
        res.add(btn_abajo);
        res.setBounds(0, 0, 200, 200);
        return res;
    }
    private void addListeners(){
        btn_izquierda.addActionListener(this);
        btn_derecha.addActionListener(this);
        btn_arriba.addActionListener(this);
        btn_abajo.addActionListener(this);
        jframe.addKeyListener(this);
    }
    @Override
    public void actualizar(Estado estado) {
        switch (estado){
            case GANADO :
                pintado("GANASTE");
                break;
            case PERDIDO:
                pintado("PERDISTE");
                break;
            case CONTINUAR:
                String cadTablero = tablero.toString();
                covertirTablero(cadTablero);
                actualizarPantalla();
                break;
        }
    }
    @Override
    public void correr() {
        //tablero.insertarNumeroDos();
        String cadTablero = tablero.toString();
        covertirTablero(cadTablero);
        actualizarPantalla();
        mostrarTablero();

    }

}

