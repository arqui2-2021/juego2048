package j2048frontend.consola;

import j2048backend.Tablero;
import j2048backend.Estado;
import j2048frontend.Observador;
import j2048frontend.TableroUI;

import java.util.Scanner;

public class TableroConsola implements Observador, TableroUI {

    private Tablero tablero;

    public TableroConsola(Tablero tablero) {
        this.tablero = tablero;
        this.tablero.agregarObservador(this);
    }


    public void imprimir(String tab){
        int k = 0 ;//replace
        String res = "";
        int tam = tab.length();
       res = tab.replace("|", "\n");
       System.out.println(res);
    }

    @Override
    public void actualizar(Estado estado) {
       switch (estado){
           case GANADO :
               System.out.println("GANASTE EL JUEGO");
               //imprimir(tablero.toString());
               break;
           case PERDIDO:
               System.out.println("PERDISTE EL JUEGO");
               //imprimir(tablero.toString());
               break;
           case CONTINUAR:
               imprimir(tablero.toString());
               System.out.println("w = arriba ; a=izquierda ; s=abajo ;d = derecha");
               break;
       }

    }

    @Override
    public void correr() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Tablero de Inicio:");
        //tablero.insertarNumeroDos();
        imprimir(tablero.toString());
        System.out.println("w = arriba ; a=izquierda ; s=abajo ;d = derecha");
        String comando= "";
        int cont = 0 ;
        boolean bb = true;
        while (bb) {
            comando = sc.nextLine();
            switch(comando){
                case "w":
                    tablero.moverArriba();
                    break;
                case "a" :
                    tablero.moverIzquierda();
                    break;
                case "s":
                    tablero.moverAbajo();
                    break;
                case "d":
                    tablero.moverDerecha();
                    break;
            }
            Estado estado = tablero.estado();
            switch (estado){
                case PERDIDO :
                    //System.out.println("PERDISTE EL JUEGO");
                    bb = false;
                    break;
                case GANADO:
                    //System.out.println("GANASTE EL JUEGO");
                    bb = false;
                    break;
                case CONTINUAR:
                    // imprimir(tablero.toString());
                    //System.out.println("w = arriba ; a=izquierda ; s=abajo ;d = derecha");
                    break;
            }
        }
    }
}
