import j2048backend.Tablero;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TableroTest {

    /*Insert*/
    @Test
    public void insertUnaVez() {
        Tablero tablero = new Tablero();
        boolean resultado = tablero.insertarNumeroDos();
        assertTrue(resultado);
    }
    @Test
    public void insert17Veces() {
        Tablero tablero = new Tablero();
        boolean res=false;
        for (int i = 0 ; i <= 16;i++){
            res = tablero.insertarNumeroDos();
        }
        assertFalse(res);
    }
    @Test
    public void tableroVacioToStirng() {
        Tablero tablero = new Tablero();
        String tab = tablero.toString();
        String res =  "0 0 0 0|0 0 0 0|0 0 0 0|0 0 0 0";
        assertEquals(tab,res);
    }
    @Test
    public void moverIzquierdaCuandoHay1ElementoCadaFila() {
        Tablero tablero = new Tablero();
        int[][] tab1 = {
                {2, 0, 0, 0},
                {0, 2, 0, 0},
                {0, 0, 2, 0},
                {0, 0, 0, 2}
        };
        String result = "2 0 0 0|2 0 0 0|2 0 0 0|2 0 0 0";
        tablero.addTablero(tab1);
        tablero.mover("Left");
        String mover = tablero.toString();
        assertEquals(mover, result);

    }
    @Test
    public void moverIzquierdaCuandoHay2ElementosCadaFila() {
        Tablero tablero = new Tablero();
        int[][] tab1 = {
                {2, 2, 0, 0},
                {0, 2, 0, 2},
                {4, 0, 2, 0},
                {2, 0, 0, 2}
        };
        String result = "4 0 0 0|4 0 0 0|4 2 0 0|4 0 0 0";
        tablero.addTablero(tab1);
        tablero.mover("Left");
        String mover = tablero.toString();
        assertEquals(mover,result);

    }

    @Test
    public void moverIzquierdaCuandoHay3ElementosCadaFila() {
        Tablero tablero = new Tablero();
        int[][] tab1 = {
                {2, 8, 2, 0},
                {4, 2, 2, 0},
                {0, 2, 2, 4},
                {8, 4, 2, 0}
        };
        String result = "2 8 2 0|4 4 0 0|4 4 0 0|8 4 2 0";
        tablero.addTablero(tab1);
        tablero.mover("Left");
        String mover = tablero.toString();
        assertEquals(mover,result);

    }
    @Test
    public void moverIzquierdaCuandoHay4ElementosCadaFila() {
        Tablero tablero = new Tablero();
        int[][] tab1 = {
                {2, 2, 2, 2},
                {4, 2, 2, 4},
                {0, 0, 0, 0},
                {8, 4, 2, 4}
        };
        String result = "4 4 0 0|4 4 4 0|0 0 0 0|8 4 2 4";
        tablero.addTablero(tab1);
        tablero.mover("Left");
        String mover = tablero.toString();
        assertEquals(mover,result);

    }

    /*derecha*/

    @Test
    public void moverDerechaCuandoHay1ElementoCadaFila() {
        Tablero tablero = new Tablero();
        int[][] tab1 = {
                {2, 0, 0, 0},
                {0, 2, 0, 0},
                {0, 0, 2, 0},
                {0, 0, 0, 2}
        };
        String result = "0 0 0 2|0 0 0 2|0 0 0 2|0 0 0 2";
        tablero.addTablero(tab1);
        tablero.mover("Right");
        String mover = tablero.toString();
        assertEquals(mover, result);

    }

    @Test
    public void moverDerechaCuandoHay2ElementosCadaFila() {
        Tablero tablero = new Tablero();
        int[][] tab1 = {
                {2, 2, 0, 0},
                {0, 2, 0, 2},
                {4, 0, 2, 0},
                {2, 0, 0, 2}
        };
        String result = "0 0 0 4|0 0 0 4|0 0 4 2|0 0 0 4";
        tablero.addTablero(tab1);
        tablero.mover("Right");
        String mover = tablero.toString();
        assertEquals(mover,result);

    }

    @Test
    public void moverDerechaCuandoHay3ElementosCadaFila() {
        Tablero tablero = new Tablero();
        int[][] tab1 = {
                {2, 8, 2, 0},
                {4, 2, 2, 0},
                {0, 2, 2, 4},
                {8, 4, 2, 0}
        };
        String result = "0 2 8 2|0 0 4 4|0 0 4 4|0 8 4 2";
        tablero.addTablero(tab1);
        tablero.mover("Right");
        String mover = tablero.toString();
        assertEquals(mover,result);

    }

    @Test
    public void moverDerechaCuandoHay4ElementosCadaFila() {
        Tablero tablero = new Tablero();
        int[][] tab1 = {
                {2, 2, 2, 2},
                {4, 2, 2, 4},
                {0, 0, 0, 0},
                {2, 2, 2, 4}
        };
        String result = "0 0 4 4|0 4 4 4|0 0 0 0|0 2 4 4";
        tablero.addTablero(tab1);
        tablero.mover("Right");
        String mover = tablero.toString();
        assertEquals(mover,result);

    }

    /*ARRIBA*/

    @Test
    public void moverArribaCuandoHay1ElementoCadaFila() {
        Tablero tablero = new Tablero();
        int[][] tab1 = {
                {0, 0, 0, 2},
                {0, 2, 0, 0},
                {0, 0, 2, 0},
                {2, 0, 0, 0}
        };
        String result = "2 2 2 2|" +
                        "0 0 0 0|" +
                        "0 0 0 0|" +
                        "0 0 0 0";
        tablero.addTablero(tab1);
        tablero.mover("Up");
        String mover = tablero.toString();
        assertEquals(mover, result);

    }
    @Test
    public void moverArribaCuandoHay2ElementoCadaFila() {
        Tablero tablero = new Tablero();
        int[][] tab1 = {
                {2, 0, 2, 0},
                {4, 2, 0, 2},
                {0, 2, 2, 0},
                {0, 0, 0, 2}
        };
        String result = "2 4 4 4|" +
                        "4 0 0 0|" +
                        "0 0 0 0|" +
                        "0 0 0 0";
        tablero.addTablero(tab1);
        tablero.mover("Up");
        String mover = tablero.toString();
        assertEquals(mover, result);

    }
    @Test
    public void moverArribaCuandoHay3ElementoCadaFila() {
        Tablero tablero = new Tablero();
        int[][] tab1 = {
                {2, 4, 0, 2},
                {4, 2, 2, 2},
                {0, 2, 2, 0},
                {8, 0, 4, 2}
        };
        String result = "2 4 4 4|" +
                        "4 4 4 2|" +
                        "8 0 0 0|" +
                        "0 0 0 0";
        tablero.addTablero(tab1);
        tablero.mover("Up");
        String mover = tablero.toString();
        assertEquals(mover, result);

    }
    @Test
    public void moverArribaCuandoHay4ElementoCadaFila() {
        Tablero tablero = new Tablero();
        int[][] tab1 = {
                {2, 4, 4, 2},
                {4, 2, 2, 2},
                {4, 2, 2, 2},
                {8, 2, 4, 2}
        };
        String result = "2 4 4 4|" +
                        "8 4 4 4|" +
                        "8 2 4 0|" +
                        "0 0 0 0";
        tablero.addTablero(tab1);
        tablero.mover("Up");
        String mover = tablero.toString();
        assertEquals(mover, result);

    }

    /*ABAJO*/
    @Test
    public void moverAbajoCuandoHay1ElementoCadaFila() {
        Tablero tablero = new Tablero();
        int[][] tab1 = {
                {2, 0, 0, 0},
                {0, 2, 0, 0},
                {0, 0, 2, 0},
                {0, 0, 0, 2}
        };
        String result = "0 0 0 0|" +
                        "0 0 0 0|" +
                        "0 0 0 0|" +
                        "2 2 2 2";
        tablero.addTablero(tab1);
        tablero.mover("Down");
        String mover = tablero.toString();
        assertEquals(mover, result);

    }
    @Test
    public void moverAbajoCuandoHay2ElementoCadaFila() {
        Tablero tablero = new Tablero();
        int[][] tab1 = {
                {2, 0, 2, 0},
                {4, 2, 0, 2},
                {0, 2, 2, 0},
                {0, 0, 0, 2}
        };
        String result = "0 0 0 0|" +
                        "0 0 0 0|" +
                        "2 0 0 0|" +
                        "4 4 4 4";
        tablero.addTablero(tab1);
        tablero.mover("Down");
        String mover = tablero.toString();
        assertEquals(mover, result);

    }
    @Test
    public void moverAbajoCuandoHay3ElementoCadaFila() {
        Tablero tablero = new Tablero();
        int[][] tab1 = {
                {2, 4, 0, 2},
                {4, 2, 2, 2},
                {0, 2, 2, 0},
                {8, 0, 4, 2}
        };
        String result = "0 0 0 0|" +
                        "2 0 0 0|" +
                        "4 4 4 2|" +
                        "8 4 4 4";
        tablero.addTablero(tab1);
        tablero.mover("Down");
        String mover = tablero.toString();
        assertEquals(mover, result);

    }
    @Test
    public void moverAbajoCuandoHay4ElementoCadaFila() {
        Tablero tablero = new Tablero();
        int[][] tab1 = {
                {2, 4, 4, 2},
                {4, 2, 2, 2},
                {4, 2, 2, 2},
                {8, 2, 4, 2}
        };
        String result = "0 0 0 0|" +
                        "2 4 4 0|" +
                        "8 2 4 4|" +
                        "8 4 4 4";
        tablero.addTablero(tab1);
        tablero.mover("Down");
        String mover = tablero.toString();
        assertEquals(mover, result);

    }

}
